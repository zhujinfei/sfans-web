
##介绍
* Sfans是一个用Java和Spring Boot开发的CMS和Blog系统。

### 核心功能
* CMS
* Blog

### 技术选型

#### 管理
* maven依赖和项目管理

#### 后端
* Java 8
* Spring Boot 1.2.3
  * Spring 4.1.6
  * Spring MVC
  * Spring security
  * Spring data-jpa
  * Spring test
  * actuator
  * liquibase
  * tomcat 8.0.20 (embed内嵌入)
  * slf4j+logback
* 缓存 ehcache

#### 前端
* webjars
  * Semantic-UI 1.8.1
  * jquery 2.1.3
  * angularjs 1.3.13
* Spring Boot
  * thymeleaf
  * html5

#### Spring Boot
 * sfans是选型Spring Boot技术的最新版本

#### 数据库
 * 目前支持mysql，建议mysql5.5及以上

###支持的浏览器
 * chrome
 * firefox
 * ie10及以上

##如何运行

####1、eclipse下运行：
* 使用Spring Tool Suite插件的eclipse
  * Run As -> Spring Boot App
  * http://localhost:8080/admin
    * 用户名 admin
    * 密码 admin

####2、用maven打包jar：
* 使用mvn package(需要依赖的jar都打包到一个jar文件内，生成了一个“胖jar”)

```
$ java -jar target/sfans-web-0.0.1-SNAPSHOT.jar
```
* 使用远程debugging支持模式

```
$ java -Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=8000,suspend=n \
       -jar target/sfans-web-0.0.1-SNAPSHOT.jar
```
