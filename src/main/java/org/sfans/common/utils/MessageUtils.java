package org.sfans.common.utils;

import org.sfans.ApplicationContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;


public class MessageUtils {

    private static MessageSource messageSource;
    
    @Autowired
    private static ApplicationContextHolder ctx;

    /**
     * 根据消息键和参数 获取消息
     * 委托给spring messageSource
     *
     * @param code 消息键
     * @param args 参数
     * @return
     */
    public static String message(String code, Object... args) {
        if (messageSource == null) {
            messageSource = ctx.getContext().getBean(MessageSource.class);
        }
        return messageSource.getMessage(code, args, null);
    }

}
