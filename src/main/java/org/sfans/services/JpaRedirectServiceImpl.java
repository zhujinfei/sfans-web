package org.sfans.services;

import java.util.Objects;

import org.sfans.domain.Redirect;
import org.sfans.domain.RedirectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JpaRedirectServiceImpl implements RedirectService
{
    @Autowired
    private RedirectRepository redirectRepository;

    @Override
    public Redirect findByUri(final String uri)
    {
        return redirectRepository.findByFromUrl(Objects.requireNonNull(uri));
    }
}
